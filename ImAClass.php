<?php
class ImAClass
{
    public $myProperty1;
    public $myProperty2;
    public $myProperty3;

    public function __construct($value)
    {
        echo $value."<br>";
    }
    public function __destruct()
    {
       echo "Good Bye"."<br>";
    }
    public static function __callStatic($name, $arguments)
    {
        echo $name."<br>";
        print_r($arguments);
    }
    public function __isset($name)
    {
        echo $name."<br>";
    }
    public function __unset($name)
    {
       echo $name."<br>";
    }
    public function __sleep()
    {
        echo "I'm inside sleep magic method"."<br>";
        return array("myProperty1",'myProperty2');
    }
    public function __wakeup()
    {
        echo "Um inside wake up"."<br>";
    }
    public function __toString()
    {
       return "Object cannot be echo"."<br>";
    }
}
ImAClass::msgBITM(55,34,"Hi there");
$obj=new ImAClass("I am inside construct function");
$result=(isset($obj->jhedwjhrliewji));

$test=serialize($obj);
echo $test;

$arr=unserialize($test);
var_dump($arr);

echo $obj;

unset($obj->djdjfdjf);
unset($obj);
echo "Hello world";
